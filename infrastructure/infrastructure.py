from aws_cdk import (
    Stack,
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_ecs_patterns as ecs_patterns,
)
from constructs import Construct


class InfrastructureStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        env_name = kwargs["env"].name

        self.vpc = ec2.Vpc.from_lookup(self, "DefaultVPC", is_default=True)

        # Create Fargate Cluster
        self.ecs_cluster = ecs.Cluster(
            self, f"FargateECS{env_name.title()}", vpc=self.vpc
        )

        # Define Docker image for the Service
        image = ecs_patterns.ApplicationLoadBalancedTaskImageOptions(
            image=ecs.ContainerImage.from_asset(directory="src")
        )

        # Create Fargate Service and ALB
        self.ecs_service = ecs_patterns.ApplicationLoadBalancedFargateService(
            self,
            f"FastAPIService{env_name.title()}",
            cluster=self.ecs_cluster,
            cpu=256,
            memory_limit_mib=512,
            desired_count=2,
            task_image_options=image,
        )
