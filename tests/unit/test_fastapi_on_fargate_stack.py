import aws_cdk as core
import aws_cdk.assertions as assertions

from infrastructure.infrastructure import InfrastructureStack


class ExtendedEnvironment(core.Environment):
    def __init__(self, account: str, name: str, region: str):
        super().__init__(account=account, region=region)
        self.name = name


# example tests. To run these tests, uncomment this file along with the example
# resource in fastapi_on_fargate/infrastructure.py
def test_sanity_check():
    app = core.App()
    stack = InfrastructureStack(
        app,
        "fastapi-on-fargate",
        env=ExtendedEnvironment(account="test", name="test", region="test"),
    )
    template = assertions.Template.from_stack(stack)

    assert template


#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
