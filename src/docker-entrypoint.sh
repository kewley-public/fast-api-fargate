#!/bin/sh

set -e

. .venv/bin/activate

# DB Upgrade example...
#while ! flask db upgrade
#do
#     echo "Retry..."
#     sleep 1
#done

exec uvicorn app:app --host 0.0.0.0 --port 80
